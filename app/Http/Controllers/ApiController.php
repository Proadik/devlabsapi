<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\House;
use GuzzleHttp\Client;

class ApiController extends Controller
{
    public function index(){
        $houses = House::all();

        if(!$houses){
            throw new HttpException(400, 'Invalid data');
        }

        return response()->json($houses, 200);
    }

    public function get($id){
        $house = House::find($id);

        if(!$house){
            throw new HttpException(400, 'Invalid data');
        }

        return response()->json($house, 200);
    }

    public function create(Request $request){
        $house = new House();
        $house->title = $request->input('title');
        $house->price = $request->input('price');
        $house->description = $request->input('description');
        $house->square = $request->input('square');
        $house->photo = $request->input('filename');
        $house->save();

        return response()->json('status', 200);
    }

}
